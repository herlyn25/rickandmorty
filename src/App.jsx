import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Routes, Route, Link } from 'react-router-dom';
import { Home } from './pages/Home';
import { Header } from './components/Navbar/Header';
import { Personajes } from './pages/Personajes';
import { Footer } from './components/Footer/Footer';

function App() {
  return (
    <div className='App'>
      <Header />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/personajes/:personajeId' element={<Personajes />} />
      </Routes>
      <Footer />
    </div>
  );
}
export default App;
