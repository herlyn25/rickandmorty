export function Buscador({valor,onBuscar}){
    return (
      <div className='d-flex justify-content-end'>
        <div className='mb-3 col-5 px-4'>
          <input
            value={valor}
            onChange={(evento) => onBuscar(evento.target.value)}
            type='text'
            className='form-control'
            placeholder='Buscar Personajes...'
          />
        </div>
      </div>
    );
}