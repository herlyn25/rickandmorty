export function Episodios({name,air_date,episode}){
    return (
      <div className='col-xl-2 col-lg-3 col-md-4 col-sm-6 col-12'>
        <div
          className='card text-dark bg-light mb-3'
          style={{ maxWidth: '18rem' }}
        >
          <div className='card-header'>{air_date}</div>
          <div className='card-body'>
            <h5 className='card-title'>{name} </h5>
            <p>{episode}</p>
          </div>
        </div>
      </div>
    );
}