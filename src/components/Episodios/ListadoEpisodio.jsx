import { Episodios } from "./Episodios";

export function ListadoEpisodio({episodios}){
    return (
      <div className='row'>
        {episodios.map((episodio) => (
          <Episodios key={episodio.id} {...episodio} />
        ))}
      </div>
    );
}