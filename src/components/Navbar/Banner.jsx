import bannerImg from '../../assets/img/banner2.jpg';
export function Banner() {
  return (
    <div className='card bg-dark text-white'>
      <img
        src={bannerImg}
        alt='Banner'
        className='card-img img-fluid rounded-start'
        style={{ height: '900px' }}
      />
    </div>
  );
}
