import { Link } from 'react-router-dom';

export function Header() {
  return (
    <nav className='navbar navbar-expand-lg navbar-dark bg-dark'>
      <div className='container-fluid font-weight-bold '>
        <Link className='navbar-brand' to='/'>
          Inicio
        </Link>
        <div className='collapse navbar-collapse' id='navbarText'>
          <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
            <li className='nav-item'>
              <Link to='/' className='nav-link active'>
                
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}