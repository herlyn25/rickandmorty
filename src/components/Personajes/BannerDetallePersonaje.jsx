function getEstilosStatus(status) {
  let color = 'green';

  if (status === 'unknown') {
    color = 'gray';
  }
  if (status === 'Dead') {
    color = 'red';
  }

  const estiloCirculo = {
    width: '10px',
    height: '10px',
    display: 'inline-block',
    backgroundColor: color,
    borderRadius: '50%',
    marginRight: '10px',
  };
  return estiloCirculo;
}

export function BannerDetallePersonaje({ name, status, species, location,image,origin}){

  return (
    <div className='card mb-3' style={{ maxWidth: '80%', marginLeft:'8%' }}>
      <div className='row g-0 bg-dark text-white'>
        <div className='col-md-12 col' style={{ height: '200px', width: '55%' }}>
          <img
            style={{ height: '100%'}}
            src={image}
            alt={name}
            className='img-fluid rounded px-2 py-2'
          />
        </div>
        <div className='col-md-5'>
          <div className='card-body'>
            <h5 className='card-title mb-0'>{name}</h5>
            <p className='text-muted'>
              <span style={getEstilosStatus(status)}></span>
              {status}-{species}
            </p>
            <p className='mb-0 text-muted'>Last Know location:</p>
            <p>{location?.name}</p>
            <p className='mb-0 text-muted'>Origin:</p>
            <p>{origin?.name}</p>
          </div>
        </div>
      </div>
    </div>
  );
}