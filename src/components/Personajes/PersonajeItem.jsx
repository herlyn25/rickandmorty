import { Link } from "react-router-dom";

function getEstilosStatus(status) {
  let color = 'green';

  if (status === 'unknown') {
    color = 'gray';
  }
  if (status === 'Dead') {
    color = 'red';
  }

  const estiloCirculo = {
    width: '10px',
    height: '10px',
    display: 'inline-block',
    backgroundColor: color,
    borderRadius: '50%',
    marginRight: '10px',
  };
  return estiloCirculo;
}

export function PersonajeItem({id,name,status,species,location,image,origin}) {
  return (
    <div className='col-lg-3 col-md-4 col-sm-6 col-12'>
      <Link
        to={`/personajes/${id}`}
        style={{ textDecoration: 'none', color: 'initial' }}
      >
        <div className='card mb-3' style={{ maxWidth: '540px' }}>
          <div className='row g-0 bg-dark text-white'>
            <div className='col-lg-3 col-md-6 col-sm-12'>
              <img src={image} alt={name} className='img-fluid rounded' />
            </div>
            <div className='col-md-8'>
              <div className='card-body '>
                <h5 className='card-title mb-0 py-0'>{name}</h5>
                <p className='text-muted'>
                  <span style={getEstilosStatus(status)}></span>
                  {status}-{species}
                </p>

                <p className='mb-0 text-muted'>Last Know location:</p>
                <p>{location?.name}</p>
                <p className='mb-0 text-muted'>Origin:</p>
                <p>{origin?.name}</p>
              </div>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
}
