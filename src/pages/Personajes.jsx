import { useEffect, useState } from 'react';
import { BannerDetallePersonaje } from '../components/Personajes/BannerDetallePersonaje';
import { ListadoEpisodio } from '../components/Episodios/ListadoEpisodio';
import axios from 'axios';
import {useParams} from 'react-router-dom'

export function Personajes() {
 let {personajeId}=useParams()
 let [personaje, setPersonajes]=useState(null)
 let [episodios,setEpisodios]=useState(null)

  useEffect(()=>{
    axios.get(`https://rickandmortyapi.com/api/character/${personajeId}`).then(response=>{
     let {episode}=response.data;
     let peticionesEpisodios=episode.map((urlEpisodio)=> axios.get(urlEpisodio));
    Promise.all(peticionesEpisodios).then((respuestaEpisodios)=>{
      let datosFormateados=respuestaEpisodios.map((episod)=>episod.data);
      setEpisodios(datosFormateados)
    })    
     setPersonajes(response.data)
    })    
  },[])
 
  console.log(episodios)  
  return (
    <div className='p-5  bg-secondary'>
      {personaje ? (
        <div>
          <BannerDetallePersonaje {...personaje} />
          <h2 className='py-4 text-white'>Episodios</h2>
          {episodios ? (
            <ListadoEpisodio episodios={episodios} />
          ) : (
            <div>cargando episodios...</div>
          )}
        </div>
      ) : (
        <div>Cargando...</div>
      )}
    </div>
  );
}